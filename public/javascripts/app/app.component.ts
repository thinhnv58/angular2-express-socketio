import {Component} from '@angular/core';

@Component({
    selector: 'my-app',
    template: `intergation demo`
})
export class AppComponent{
    socket = null;
    constructor(){
        this.socket = io();
    }
}